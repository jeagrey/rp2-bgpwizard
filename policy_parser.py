import re
from xml.etree.ElementTree import Element, SubElement, tostring

def from_policy(frm_str):
    #Gets words from the "from" statement excluding action/accept
    elem = Element('import')
    fr = ""
    replace = ["(",")"]
    for i in replace:
	frm_str = frm_str.replace(i, "")
    if "refine" in frm_str:
        frm_str = frm_str.split("refine")[0]
    if "action" in frm_str:
        fr = re.findall( 'from (.*?) action', frm_str.strip())[0]
    else:
        fr = re.findall( 'from (.*?) accept', frm_str.strip())[0]
    elem = get_ip(fr, elem, 'from')
    return elem

def get_ip(frm_str, elem, action): # Action = from/to
    # Check if [0] is an AS-SET!
    data = frm_str.split(" ")
    elem.set(action, data[0])
    if " at " in frm_str and len(data) == 4:
	elem.set('remote_ip', data[1])
	elem.set('local_ip', data[3])
	return elem
    if len(data) == 2 and re.search('^\d+', data[1]):
	elem.set('remote_ip', data[1])
    return elem

def action_policy(act_str):
    #Gets words betweein the "action" statement and "accept"
    if "refine" in act_str:
        act_str = act_str.split("refine")[0]
    if "action" in act_str:
        elem = Element('action')
	act = ''
	if "accept" in act_str:
            act = re.findall( 'action (.*?) accept', act_str.strip())[0]
	if "announce" in act_str:
	    act = re.findall( 'action (.*?) announce', act_str.strip())[0]
	elem = get_action(act, elem)
        return elem
    return None

def get_action(act_str, elem):
    med_elem = Element("med")
    pref_elem = Element("pref")
    community_elem = Element("community")
    if "pref" in act_str:
	pref_elem.text = re.findall( 'pref=(\d+)', act_str.strip())[0]
	elem.append(pref_elem)
    if "community" in act_str:
	if "community.=" in act_str:
	    community_elem.text = re.findall( 'community.=(.*?) ', act_str.strip())[0]
	if "community.append" in act_str:
            community_elem.text = re.findall( 'community.append\((.*?)\)', act_str.strip())[0]
	elem.append(community_elem)
    if "med" in act_str:
	med_elem.text = re.findall( 'med=(\d+)', act_str.strip())[0]
	elem.append(med_elem)
    return elem

def accept_policy(acc_str):
    #Gets words between the "action" statement and "accept"
    replace = ["(",")"]
    for i in replace:
        acc_str = acc_str.replace(i, "")
    if "refine" in acc_str:
        acc_str = acc_str.split("refine")[0]
    if "accept" in acc_str:
        elem = Element('accept')
        elem.text = re.findall( 'accept (.*)', acc_str.strip(), re.DOTALL)[0]
        return elem
    return None

def refine_policy(ref_str):
    #Gets words between the "action" statement and "accept"
    sp = ""
    if "refine" in ref_str:
        sp = ref_str.replace("\n", "").split("refine")
    else:
	return None
    elems = []
    for i in sp[1:]:
	elem = Element("refine")
	ref_s = i.strip().split("from")
	#Start from 1, ignore leading space from split
	for r in ref_s[1:]:
	    r = "from" + r
	    elem.append(from_policy(r))
	    action = action_policy(r)
            accept = accept_policy(r)
	    if action != None:
		elem.append(action)
	    if accept != None:
		elem.append(accept)
	elems.append(elem)
    return elems

def to_policy(to_str):
    #Gets words from the "to" statement excluding action/accept
    elem = Element('export')
    fr = ""
    replace = ["(",")"]
    for i in replace:
        to_str = to_str.replace(i, "")
    if "refine" in to_str:
        to_str = to_str.split("refine")[0]
    if "action" in to_str:
        fr = re.findall( 'to (.*?) action', to_str.strip())[0]
    else:
        fr = re.findall( 'to (.*?) announce', to_str.strip())[0]
    elem = get_ip(fr, elem, 'for')
    return elem

def ann_policy(ann_str):
    elem = Element('announce')
    replace = ["(",")"]
    for i in replace:
        ann_str = ann_str.replace(i, "")
    elem.text = re.findall( 'announce (.*)', ann_str.strip(), re.DOTALL)[0] 
    return elem