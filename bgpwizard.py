import re
from collections import defaultdict
import policy_extract, policy_parser, nc, xml_gen
from xml.etree.ElementTree import Element, tostring, fromstring, parse
from xml.dom import minidom
from ncclient.xml_ import *
import filters

def rpsl_med(i):
    pol_then = []
    search = './router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"][@ip="'+remote_ip+'"]/import_policy/med'
    if local_file.findall(search) != []:
        if local_file.findall(search)[0].text != 'None':
	    pol_then.append('metric ' + local_file.findall(search)[0].text)
    else:
        if i.findall('./action/med') != []:
            med = i.findall('./action/med')[0].text
            pol_then.append('metric ' + med)
    return pol_then

def rpsl_pref(i):
    pol_then = []
    search = './router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"][@ip="'+remote_ip+'"]/import_policy/lpref'
    if local_file.findall(search) != []:
        if local_file.findall(search)[0].text != 'None':
            pol_then.append('local-preference ' + local_file.findall(search)[0].text)
    else:
        if i.findall('./action/pref') != []:
       	    pref = i.findall('./action/pref')[0].text
            lpref = str(65535 - int(pref)) #Comliance of RFC 2622
            pol_then.append('local-preference ' + lpref)
    return pol_then

def rpsl_community(i):
    pol_then = []
    if i.findall('./action/community') != []:
        community = i.findall('./action/community')[0].text
        community_name = 'rpsl_community' # get from local file or create auto
        if action == 'import':
            import_policies.append(nc.community(community_name, community))
            import_policy.append(pol_name_comunity)
            import_policies.append(nc.policy_statement(pol_name_community, then_string=['accept'], from_string='community + ' + community_name))
        else:
            export_policies.append(nc.community(community_name, community))
            export_policy.append(pol_name_community)
    return pol_then

def get_accept(i, v6=False):
    accept=i.findall('./accept')[0].text
    if ("as" in accept) or ("rs" in accept):
	if len(accept.split(" ")) > 1:
	    for j in accept.split(" "):
		if ("as" in j) or ("rs" in j):
		    check_route(j.upper(), 'import', v6=v6)
	else:
            check_route(accept.upper(), 'import', v6=v6)
    if accept[0].isdigit():
        pass#eate prefix list for network

def get_announce(i, v6=False):
    announce=i.findall('announce')[0].text
    if announce == 'any':
        if 'ANNOUNCE_ANY' not in route_filters:
            export_policies.append(nc.policy_statement("ANNOUNCE_ANY", then_string=['accept'], from_string='protocol direct protocol local protocol bgp'))
            route_filters.append('ANNOUNCE_ANY')
	export_policy.append('ANNOUNCE_ANY')
    if ("as" in announce and announce!= 'as-telianet') or ("rs" in announce):
        check_route(announce.upper(), 'export', v6=v6)
    if announce[0].isdigit():
        pass
        #create prefix list for network

def check_route(filt, action, v6=False):
    q3 = ''
    if filt not in route_filters:
        q3 = nc.q3_query(filt, filt, True, j=True, v6=v6)
        if action == 'import' and len(q3.split())<85000:
            import_policies.append('\n'.join(q3.split('\n')[1:-3])+ '\nthen accept; }\n')
        if action == 'export' and len(q3.split())<85000:
            export_policies.append('\n'.join(q3.split('\n')[1:-3])+ '\nthen accept; }\n')
        route_filters.append(filt)
    if action == 'import' and len(q3.split())<85000:
        import_policy.append(filt)
    else:
	if len(q3.split())<85000:
            export_policy.append(filt)

def policy_gen(pol_then, action):
    if pol_then != []:
	if action == 'import':
            import_policies.append(nc.policy_statement(import_policy_name, then_string=pol_then))
            import_policy.append(import_policy_name)
	else:
            export_policies.append(nc.policy_statement(export_policy_name, then_string=pol_then))
            export_policy.append(export_policy_name)

def config_gen(action, version):
    pol_then = []
    neighbor = ''
    if action == 'import':
	neighbor = '[@from=\"'+'as'+asn+'\"]'
    if action == 'export':
        neighbor = '[@for=\"'+'as'+asn+'\"]'
    for i in frm.findall('.'+action+'s/'+action+neighbor):
	rpsl_remote = i.attrib.get("remote_ip")
	rpsl_local = i.attrib.get("local_ip")
	#Correct addresses exist in the RPSL data and match the current check
	#Empty addresses means use current IP addresses from local file
	correct_addresses = (rpsl_remote==remote_ip and (rpsl_local == None or rpsl_local==public_ip))
        empty_addresses = (rpsl_local==None and rpsl_remote==None)
	if (correct_addresses or empty_addresses): #check local IP in RPSL
	    bogon_filter_name = 'BOGON_FILTER'+version
	    if bogon_filter_name not in security_filters:
	        bogons = filters.get_source('fullbogons-ipv'+version+'.txt')
	        fil = filters.create_rf(bogons, 'orlonger')
	        pol = nc.policy_statement(bogon_filter_name, then_string=['reject'], from_string=fil)
	        import_policies.append(pol)
	        security_filters.append(bogon_filter_name)
	    if rpsl_med(i) != []:
	        pol_then.extend(rpsl_med(i))
            if rpsl_pref(i) != []:
	        pol_then.extend(rpsl_pref(i))
            if rpsl_community(i) != []:
	        pol_then.extend(rpsl_community(i))
	    policy_gen(pol_then, action)
	    if action == 'import':
		if version=="4":
		    get_accept(i, v6=False)
		else: get_accept(i, v6=True)
            if action == 'export':
                if version=="4":
                    get_announce(i, v6=False)
		else: get_announce(i, v6=True)

def parse_neighbors(all_neighbors, v6=False):
    for i in set(all_neighbors):
        global import_policy
        global export_policy
	global asn, import_policy_name, export_policy_name
	global remote_ip
	import_policy = []
        export_policy = []
        import_policy_name = None #Get names from local file
        export_policy_name = None
        asn = i.split("#")[0].replace("as", "")
        remote_ip = i.split("#")[1]
        search = './router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"][@ip="'+remote_ip+'"]'
        for p in local_file.findall(search):
            if p.findall('./import_policy/name') != []:
                import_policy_name = p.findall('./import_policy/name')[0].text
        if import_policy_name == None:
            import_policy_name = asn + '_import_' + remote_ip
        if export_policy_name == None:
            export_policy_name = asn + '_export_' + remote_ip
	if v6==True:
	    config_gen('import', "6")
            config_gen('export', "6")
	else:
            config_gen('import', "4")
            config_gen('export', "4")
	group = "bgpwizard_group"
        if local_file.findall(search + '[@group]') != []:
            for p in local_file.findall(search + "[@group]"):
		group = p.get('group')
        import_policy = security_filters + import_policy
        export_policy = security_filters + export_policy
        neighbor = nc.add_neighbor(asn, remote_ip, import_policy, export_policy)
        if (group, neighbor) not in groups:
            groups.append((group, neighbor))
        if ([public_ip, groups]) not in router_groups:
            router_groups.append([public_ip, groups])


local_file = xml_gen.get_localfile() 
routers = [] # get list from file first

for rtr in local_file.findall('./router'):
    routers.append(rtr.attrib.get('mgmt_ip'))
for mgmt_ip in routers:
    source_asn = local_file.findall('./router[@mgmt_ip="'+mgmt_ip+'"]')[0].attrib.get('asn')
    root = xml_gen.get_xml('AS'+source_asn)
    frm = fromstring(tostring(root))
    public_ip = local_file.findall('./router[@mgmt_ip="'+mgmt_ip+'"]')[0].attrib.get('public_ip')
    route_filters = []
    import_policies = []
    export_policies = []
    security_filters = []
    groups = []
    router_groups = []
    all_neighbors = []
    for j in frm.findall('./imports/import'):
	asn = j.attrib.get('from').replace("as","")
	#if no remote_ip, then look for AS in local file and get IP, else IGNORE
	if j.attrib.get('remote_ip') != None:
	    if j.attrib.get('local_ip') != None:
	        loc_ip = j.attrib.get('local_ip')
	    else:
		search = './router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"]'
		for i in local_file.findall(search):
		    if i.attrib.get("ip") != None:
            		#if local_ip has that AS - get remote IP from there
            		# Here goes file IP or break
                        all_neighbors.append(asn+'#'+i.attrib.get("ip") + "#" + public_ip)
		    else: break
	    if loc_ip == public_ip: # or from file?
	        all_neighbors.append(asn+'#'+j.attrib.get('remote_ip')+"#"+public_ip)
	else:
	    search = 'router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"]'
	    for i in local_file.findall(search):
		if i.attrib.get("ip") == None:
		    break
		else:
		    all_neighbors.append(asn+'#'+i.attrib.get("ip") + "#" + public_ip)
    for j in frm.findall('./exports/export'):
	asn = j.attrib.get('for').replace("as","")
        if j.attrib.get('remote_ip') != None:
            if j.attrib.get('local_ip') != None:
                loc_ip = j.attrib.get('local_ip')
            else:
                search = './router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"]'
		for i in local_file.findall(search):
                    if i.attrib.get("ip") != None:
                        all_neighbors.append(asn+'#'+i.attrib.get("ip")+"#"+public_ip) 
            if loc_ip == public_ip:
                all_neighbors.append(asn+'#'+j.attrib.get('remote_ip')+"#"+public_ip)
        else:
            search = './router[@public_ip="'+public_ip+'"]/neighbor[@asn="'+asn+'"]'
	    for i in local_file.findall(search):
                if i.attrib.get("ip") != None:
                    all_neighbors.append(asn+'#'+i.attrib.get("ip")+ "#" + public_ip)
    parse_neighbors(all_neighbors, v6=False)
    all_pols = "\n".join(import_policies) + "\n".join(export_policies)
    all_pols.replace('\n\n', '\n')
    neighbors = ''
    for i in router_groups:
	local_address = i[0]
        grs = defaultdict(list)
        for gr, nei in groups:
            grs[gr].append(nei)
        for gr, neigs in grs.iteritems():
	    neighbors = neighbors + nc.add_group(gr, "".join(neigs), local_address)
    config = nc.add_protocol(neighbors)
    config = config + nc.add_pol_options(all_pols)
    username = local_file.findall('./router[@mgmt_ip="'+mgmt_ip+'"]')[0].attrib.get('username')
    password = local_file.findall('./router[@mgmt_ip="'+mgmt_ip+'"]')[0].attrib.get('password')
    conn = nc.connect(mgmt_ip, 22, username, password, 'candidate')
    conn.lock()
    try:
	conn.load_configuration(format="text", config=config)
	conn.commit()
    finally:
	conn.discard_changes()
	conn.unlock()
	conn.close_session()
