import httplib, sys
import xml.etree.ElementTree as et

def get_rpsl(asn):
    try:
	conn = httplib.HTTPSConnection("rest.db.ripe.net")
	url = "/search?type-filter=aut-num&source=ripe&query-string=" + asn
	conn.request("GET", url)
	resp = conn.getresponse()
	conn.close()
	if resp.status != 200:
	    print 'HTTP status', resp.status
	    sys.exit()
	return et.fromstring(resp.read())
    except:
	print 'Something went wrong with the connection to RIPE. Please try again!'
	sys.exit()

def get_policy(xml):
    ipv4_import = []
    ipv4_export = []
    ipv6_import = []
    ipv6_export = []
    for i in xml.findall('./objects/object[@type="aut-num"]/attributes/attribute[@name="import"]'):
	if i not in ipv4_import:
            ipv4_import.append(i.attrib.get("value"))
	else: print "found a double!"
    for i in xml.findall('./objects/object[@type="aut-num"]/attributes/attribute[@name="export"]'):
	if i not in ipv4_export:
            ipv4_export.append(i.attrib.get("value"))
    for i in xml.findall('./objects/object[@type="aut-num"]/attributes/attribute[@name="mp-import"]'):
	if i not in ipv6_import:
            ipv6_import.append(i.attrib.get("value"))
    for i in xml.findall('./objects/object[@type="aut-num"]/attributes/attribute[@name="mp-export"]'):
	if i not in ipv6_import:
            ipv6_export.append(i.attrib.get("value"))
    return ipv4_import, ipv4_export, ipv6_import, ipv6_export
