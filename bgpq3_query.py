import subprocess

def query(asn, name, agg, j=False, v6=False):
    if not v6:
	if agg and j:
            cmd = 'bgpq3 -JEAl '+ name + ' ' + asn
	elif j:
	    cmd = 'bgpq3 -Jl '+ name + ' ' + asn
	elif agg:
	    cmd = 'bgpq3 -EAl '+ name + ' ' + asn
	else:
	    cmd = 'bgpq3 -l '+ name + ' ' + asn
    else:
	if agg and j: #Juniper  output with aggregation
	    cmd = 'bgpq3 -JE6l '+ name + ' ' + asn
	elif j: # Juniper output without aggregation
            cmd = 'bgpq3 -J6l '+ name + ' ' + asn
	elif agg: #Cisco output with aggregation
	    cmd = 'bgpq3 -EA6l '+ name + ' ' + asn
	else: #Cisco output, no aggregation
	    cmd = 'bgpq3 -6l '+ name + ' ' + asn
    return subprocess.check_output(cmd, shell=True)
