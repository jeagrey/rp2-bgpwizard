from xml.etree.ElementTree import Element, tostring, fromstring
from xml.dom import minidom
import policy_parser, policy_extract, yaml2xml

def get_localfile():
    return yaml2xml.get_localfile()

def get_xml(asn):
    xml = policy_extract.get_rpsl(asn)
    ipv4_import, ipv4_export, ipv6_import, ipv6_export = policy_extract.get_policy(xml)[:]
    root = Element('root')
    root.append(get_import(ipv4_import))
    root.append(get_import(ipv6_import, v6=True))
    root.append(get_export(ipv4_export))
    root.append(get_export(ipv6_export, v6=True))
#    print minidom.parseString(tostring(root)).toprettyxml()
    return root

def get_import(p_import, v6=False):
    root = ''
    if v6:
	root = Element('v6imports')
    else:
	root = Element('imports')
    for i in set(p_import):
        replace = [";","{", "}"]
        for r in replace:
            i = i.replace(r, "")
        i = i.lower()
        import_tag = policy_parser.from_policy(i)
        action = policy_parser.action_policy(i)
        accept = policy_parser.accept_policy(i)
        refine = policy_parser.refine_policy(i)
        if action != None:
            import_tag.append(action)
        import_tag.append(accept)
        if refine != None:
            for ref in refine:
                import_tag.append(ref)
        root.append(import_tag)
    return root

def get_export(p_export, v6=False):
    if v6:
        root = Element('v6exports')
    else:
	root = Element('exports')
    for i in set(p_export):
        replace = [";","{", "}", "(", ")"]
        for r in replace:
            i = i.replace(r, "")
        i = i.lower()
        export_tag = policy_parser.to_policy(i)
        action = policy_parser.action_policy(i)
        announce = policy_parser.ann_policy(i)
        refine = policy_parser.refine_policy(i)
        if action != None:
            export_tag.append(action)
        export_tag.append(announce)
        if refine != None:
            for ref in refine:
                export_tag.append(ref)
        root.append(export_tag)
    return root
