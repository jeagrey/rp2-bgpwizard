#!/usr/bin/env python
from ncclient import manager
from ncclient.xml_ import *
import bgpq3_query

def q3_query(asn, name, agg, j=False, v6=False):
    return bgpq3_query.query(asn, name, agg, j=j, v6=v6)

def connect(host, port, user, password, source):
    conn = manager.connect(host=host,
            port=port,
            username=user,
            password=password,
            timeout=400,
            device_params = {'name':'junos'},
            hostkey_verify=False)
    return conn

def get_conf(conn):
    return conn.get_configuration(format='xml')

def add_protocol(groups):
    return '''protocols {
    bgp {''' +groups + '''
    }
}'''
def add_group(name, neighbors, addr):
    return '''
	group ''' + name + ''' {
	    local-address ''' + addr + ''';
 	    type external;''' +  neighbors + '''
	}''' 

def add_pol_options(policies):
    return '''
policy-options {''' + policies + '''
}'''

def add_neighbor(asn, remote_ip, import_p=None, export_p=None):
    config = '''
	    neighbor '''+remote_ip+''' {'''
    if import_p != []:
	if len(import_p)==1:
            config = config + '\n                import '+' '.join(import_p)+';'
	else:
            config = config + '\n                import ['+' '.join(import_p)+'];'
    if export_p != []:
	if len(export_p) == 1:
            config = config + '\n                export '+' '.join(export_p)+';'
	else:
	    config = config + '\n                export ['+' '.join(export_p)+'];'
    config = config + '''\n                peer-as '''+asn+''';
            }'''
    return config

def policy_statement(name, then_string=None, from_string=None):
    config = '''
    policy-statement ''' + name + ' { '
    if from_string != None:
        config = config + '\nfrom{ ' + from_string + ';}'
    if then_string != None:
	for i in then_string:
	    config = config + '\nthen{ ' + i + ';}'
    config = config + "\n    }"
    return config

def community(name, members):
    config = 'community ' + name + ' members ' + members + ";"
    return config 