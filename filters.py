import urllib2
import os.path

def get_source(filename):
    url = 'https://www.team-cymru.org/Services/Bogons/' + filename
    response = urllib2.urlopen(url).read()
    return response.split('\n')

def create_rf(routes, action):
    routes_string = ''
    for r in routes[1:]:
	if r != '':
	    new_route = 'route-filter ' + r + ' ' + action + ';'
	    if routes_string == '':
		routes_string = new_route
	    else:
	    	routes_string = routes_string + '\n' + new_route
    return routes_string 