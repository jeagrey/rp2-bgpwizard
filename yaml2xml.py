import yaml
from lxml import etree as et 

def yaml_loader(filename):
    with open(filename, "r") as f:
        data = yaml.load(f)
        return data

def get_localfile():
    data = yaml_loader("local.yaml")
    root = et.Element('root')
    for rtr in data:
        #Router element created
        router_ip = rtr['public_ip']
        router_name = rtr['name']
        router_user = rtr['username']
        router_mgmt_ip = rtr['mgmt_ip']
        router_asn = str(rtr['asn'])
        router_pw = rtr['password']
        router = et.SubElement(root, "router", public_ip=router_ip, name=router_name, username=router_user, password=router_pw, mgmt_ip=router_mgmt_ip, asn=router_asn) 

        #Iteration for each negihbor
        try: 
            for i in rtr['neighbors']:
                temp = i.keys()[0]
                neighbor = et.SubElement(router, "neighbor", asn=str(i[temp]['as']), group=i[temp]['group'], ip=temp)
                #Import policy
                import_policy = et.SubElement(neighbor, "import_policy")

                use_RPSL2 = et.SubElement(import_policy, "use_rpsl")
                use_RPSL2.text = str(i[temp]['import_policy']['use_RPSL'])

                name2 = et.SubElement(import_policy, "name")
                name2.text = i[temp]['import_policy']['name']

                lpref2 = et.SubElement(import_policy, "lpref")
                lpref2.text = str(i[temp]['import_policy']['lpref'])

                med2 = et.SubElement(import_policy, "med")
                med2.text = str(i[temp]['import_policy']['med'])

                community =  et.SubElement(import_policy, "community")
                com_name_import = et.SubElement(community, "name")
                com_name_import.text = i[temp]['import_policy']['community']['name']

                com_string_import = et.SubElement(community, "string")
                com_string_import.text = i[temp]['import_policy']['community']['string']

                com_policy_name = et.SubElement(community, "policy_name")
                com_policy_name.text = i[temp]['import_policy']['community']['policy_name']
                #Export Policy
                export_policy = et.SubElement(neighbor, "export_policy")

                use_RPSL4 = et.SubElement(export_policy, "use_rpsl")
                use_RPSL4.text = str(i[temp]['export_policy']['use_RPSL'])

                name4 = et.SubElement(export_policy, "name")
                name4.text = i[temp]['export_policy']['name']
                 
                lpref4 = et.SubElement(export_policy, "lpref")
                lpref4.text = str(i[temp]['export_policy']['lpref'])
                
                med4 = et.SubElement(export_policy, "med")
                med4.text = str(i[temp]['export_policy']['med'])

                community_ex =  et.SubElement(import_policy, "community")
                com_name_export = et.SubElement(community_ex, "name")
                com_name_export.text = i[temp]['export_policy']['community']['name']
                com_string_export = et.SubElement(community_ex, "string")
                com_string_export.text = i[temp]['export_policy']['community']['string']
                com_policy_name_export = et.SubElement(community_ex, "policy_name")
                com_policy_name_export.text = i[temp]['export_policy']['community']['policy_name']

                #No import
                no_import = et.SubElement(neighbor, "no_import")
                no_import.text = i[temp]['no_import']
                #No export
                no_export = et.SubElement(neighbor, "no_export")
                no_export.text = i[temp]['no_export']
                #RPKI
                rpki = et.SubElement(neighbor, "rpki")
                rpki.text = str(i[temp]['RPKI'])
                #RPKI Actions
                rpki_actions = et.SubElement(neighbor, "rpki_actions")

                discard = et.SubElement(rpki_actions, "discard")
                discard.text = str(i[temp]['RPKI_actions']['discard'])

                lpref7 = et.SubElement(rpki_actions, "lpref")
                lpref7.text = str(i[temp]['RPKI_actions']['lpref'])
                #Logical System
                logical_system =  et.SubElement(neighbor, "logical_system")
                logical_system.text = i[temp]['logical_system']
        except:
	    pass
    return et.ElementTree(root)
